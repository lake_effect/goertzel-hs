import Goertzel
import Data.Complex
import Data.Fixed
import Test.Hspec
-- import Test.QuickCheck

import Debug.Trace

{-

Tests are derived from this paper excerpt: 
http://dsp.stackexchange.com/a/635/5614

-}

tau = 2 * pi

-- Util function adapted from https://rust-bio.github.io/rust-bio/src/itertools_num/linspace.rs.html#78-93
linspace :: Double -> Double -> Int -> [Double]
linspace x y n = do
  let step | n > 1 = (y - x) / (fromIntegral (n - 1))
           | otherwise = 0
      nums = [0..n-1]
    in map (\ind -> (fromIntegral ind) * step) nums

first_difference :: [Double] -> [Double]
first_difference input = do
  let minuend_list = init input
  let subtrahend_list = tail input
  map (\(subtrahend, minuend) -> minuend - subtrahend) (zip minuend_list subtrahend_list)

replace_at :: [a] -> a -> Int -> [a]
replace_at list input index = do
  let (top, _:bottom) = splitAt index list
  top ++ input : bottom

-- Shows the input, then returns it
trace_retsh :: Show a => a -> a
trace_retsh thing = trace (show thing) thing

main :: IO ()
main = hspec $ do
  describe "DFT" $ do
    it "is linear-additive" $ do
      let freq = 440.0
          interval_length = 1.0
          delta = interval_length / freq / 2.0
          time_1s = map (\sample -> sample * delta) (linspace 0.0 interval_length (floor (freq / 2)))
          sine_440 = (map (\time_sample -> (sin (time_sample * freq))) time_1s)
          sine_100 = map (\time_sample -> (sin (time_sample * 100.0))) time_1s
          summed_signal = map (\(a, b) -> a + b) (zip sine_440 sine_100)
          dft_440 = Goertzel.dft 440.0 sine_440
          dft_100 = Goertzel.dft 440.0 sine_100
          summed_signal_dft = Goertzel.dft 440.0 summed_signal
          summed_dft = dft_440 + dft_100
          complex_diff = summed_dft - summed_signal_dft
       in (abs (imagPart complex_diff) < 0.00001)
          && (abs (realPart complex_diff) < 0.00001)
          `shouldBe` True

    it "is linear in root power" $ do
      let freq = 440.0
          interval_length = 1.0
          delta = interval_length / freq / 2.0
          time_1s = map (\sample -> sample * delta) (linspace 0.0 interval_length (floor (freq / 2)))
          sine_440 = map (\time_sample -> (sin (time_sample * freq))) time_1s
          sine_100 = map (\time_sample -> (sin (time_sample * 100.0))) time_1s
          summed_signal = map (\(a, b) -> a + b) (zip sine_440 sine_100)
          power_440 = Goertzel.dft_power 440.0 sine_440
          power_100 = Goertzel.dft_power 440.0 sine_100
          summed_signal_power = Goertzel.dft_power 440.0 summed_signal
          summed_power = ((sqrt power_440) + (sqrt power_100) ** 2)
       in (abs (summed_power - summed_signal_power) < 0.1) 
          `shouldBe` True

    it "is constant for impulses" $ do
      let max_freq = 500.0
          interval_length = 0.5
          delta = interval_length / max_freq / 2.0
          time_500ms = map (\sample -> sample * delta) (linspace 0.0 interval_length (floor (max_freq / 2.0)))

          impulse_500ms = [1.0] ++ (tail time_500ms) :: [Double]

          dft_bins = [1.0, 10.0, 100.0, 250.0, 300.0, 500.0]

          impulse_dfts = map (\dft_freq -> Goertzel.dft dft_freq impulse_500ms) dft_bins
          asserts = (zipWith
                      (\x y -> (magnitude x - magnitude y) < 0.01)
                      impulse_dfts
                      (tail impulse_dfts))
        in (all (\x -> x == True) asserts) `shouldBe` True          

    it "finds constant power for impulses" $ do
      let max_freq = 500.0
          interval_length = 0.5
          delta = interval_length / max_freq / 2.0
          time_500ms = map (\sample -> sample * delta) (linspace 0.0 interval_length (floor (max_freq / 2.0)))

          impulse_500ms = [1.0] ++ (tail time_500ms) :: [Double]

          dft_bins = [1.0, 10.0, 100.0, 250.0, 300.0, 500.0]

          impulse_dfts = map (\dft_freq -> Goertzel.dft_power dft_freq impulse_500ms) dft_bins
          asserts = (zipWith
                      (\x y -> (abs $ (x - y)) < 0.001)
                      impulse_dfts
                      (tail impulse_dfts))
        in (all (\x -> x == True) asserts) `shouldBe` True

    it "is constant in phase difference" $ do
      let max_freq = 500.0
          interval_length = 0.5
          delta = interval_length / max_freq / 2.0
          time_500ms = map (\sample -> sample * delta) (linspace 0.0 interval_length (floor (max_freq / 2.0)))

          offset_count = 15
          offset_length = (length time_500ms) `div` offset_count

          offset_impulses = tail $ map (\offset_ind -> replace_at time_500ms 1.0 offset_ind)
                            [1,2..offset_count]

          dft_freq = 500.0

          impulse_dfts = map (\impulse -> Goertzel.dft dft_freq impulse) offset_impulses

          dft_phases = map (\dft -> ((phase dft) `mod'` tau) + tau)
                       impulse_dfts

          dft_phase_diffs =  map (\diff -> (diff + tau) `mod'` tau)
                            (first_difference dft_phases)
          asserts = (zipWith
                      (\x y -> (abs $ (x - y)) < 0.1)
                      dft_phase_diffs
                      (tail dft_phase_diffs))
        in (all (\x -> x == True) asserts) `shouldBe` True
