module Goertzel
    ( filter_naive,
      dft,
      dft_power,
      intermediate_filter
    ) where

import Data.Complex

filter_recur :: Floating f => f -> [f] -> [f] -> [f]
filter_recur freq_term input acc
  -- init filter #1: initialize the output vec with input[0]
  | length acc == 0 = filter_recur freq_term (tail input) [head input]
  -- init filter #2: initialize second term of output vec with input[1] + freq * input[0]
  | length acc == 1 = filter_recur freq_term (tail input) (acc ++ [freq_term * (head acc)])
  -- We're done once we've eaten the input, just return
  | length input == 0 = acc
  -- The actual recursion: s[n]=x[n] + 2\cos(\omega _{0}) * s[n-1]-s[n-2]
  | otherwise = filter_recur freq_term (tail input)
                (acc ++ [(head input) + freq_term * (last acc) - (last (init acc))])

intermediate_filter :: Floating f => f -> [f] -> [f]
intermediate_filter angular_freq input =
  filter_recur freq_term input []
  where freq_term = 2.0 * cos(angular_freq)

bin_freq :: (Floating f, RealFrac f, Integral i) => f -> i -> (i, f)
bin_freq linear_freq signal_length = do
  let angular_freq = linear_freq / 2.0 / pi
      approx_bin_num = angular_freq * fromIntegral signal_length / 2.0 / pi
      bin_num = truncate approx_bin_num
      freq = (fromIntegral bin_num) / (fromIntegral signal_length) * 2.0 * pi
    in (bin_num, freq)

naive_recur :: RealFloat f => f -> [f] -> [Complex f] -> Complex f -> Complex f -> [Complex f]
naive_recur angular_freq input acc exp_term old_last_term
  | length(acc) == 0 = do
      let intermediate_vec = intermediate_filter angular_freq input
          j = 0 :+ 1
          exp_term = exp (-j * (to_cmplx angular_freq))
          last_term = to_cmplx (head intermediate_vec)
        in naive_recur angular_freq intermediate_vec [(to_cmplx (head intermediate_vec))] exp_term last_term
  | length(input) > 1 = do
      let last_term = (to_cmplx (head input))
        in naive_recur angular_freq (tail input) (acc ++ [(to_cmplx (head input)) - exp_term * old_last_term]) exp_term last_term

filter_naive :: RealFloat f => f -> [f] -> [Complex f]
filter_naive freq input = do
  let angular_freq = freq / 2.0 / pi
      in naive_recur angular_freq input [] 0 0

to_cmplx :: Floating f => f -> Complex f
to_cmplx num = num :+ 0

dft :: RealFloat f => f -> [f] -> Complex f
dft linear_freq input = do
  let (_, freq) = bin_freq linear_freq (length input)
      intermediate_vec = (init (intermediate_filter freq input))
      state_1 = last intermediate_vec
      state_2 = last $ init intermediate_vec
      j = (0 :+ 1)
      exp_term = exp (j * (freq :+ 0))

      in exp_term * (state_1 :+ 0) - (state_2 :+ 0)

dft_power :: (Show f, Floating f, RealFrac f) => f -> [f] -> f
dft_power linear_freq input = do
  let (_, freq) = bin_freq linear_freq (length input)
      intermediate_vec = init (intermediate_filter freq input)
      state_1 = last (intermediate_vec)
      state_2 = last (init intermediate_vec)

      in state_1 ^^ 2 + state_2 ^^ 2 - 2 * cos(freq) * state_1 * state_2
