# goertzel-hs

Haskell implementation of
the [Goertzel filter](https://en.wikipedia.org/wiki/Goertzel_algorithm).

The Goertzel filter is faster than the FFT for a small range of frequencies of
interest.

## interface

- `filter_naive :: RealFloat f => f -> [f] -> [Complex f]`
    
    Just applies the filter equations and gives you the complete output.

- `dft :: RealFloat f => f -> [f] -> Complex f`
    
    Computes the associated (closest) DFT term to a given linear frequency.

- `dft_power :: (Floating f, RealFrac f) => f -> [f] -> f`
    
    Computes the power of the signal at a given linear frequency.

## TODO

- Docs
